$(function() {
	$(document).on('scroll', function(){
		var topOffset = $(window).scrollTop(),
			notAnimated = true,
			blockOffset = $('.b-lets-work').offset().top;
		if (topOffset>(blockOffset-300)&&notAnimated) {
			notAnimated = false;
			$('.b-lets-work__rocket').addClass('b-lets-work__rocket_animate_yes');
		}
	});
});