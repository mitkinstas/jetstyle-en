$(function () {
    if ($('.popup-show').length) {
        $('.popup-show').on('click', function () {
            showAndHidePopup(false, true);

        });

        $('.popup-close').on('click', function () {
            event.stopPropagation();
            showAndHidePopup(true);
        });

        $('.b-popup__wrp').on('click', function (event) {

            if ($(event.target).closest('.b-popup__content').length === 0 &&
                $(event.target).closest('.b-popup__thank-content').length === 0 &&
                $(event.target).closest('.b-popup__subscr-content').length === 0) {
                showAndHidePopup(true);
            }

        });
        $(document).on('keyup', function (e) {
            if (e.keyCode === 27) {
                showAndHidePopup(true);
            }
        });

        $('.b-popup__form').on('submit', function() {
            var form = $(this);
            var formData = new FormData(form[0]);

            if ($('[name="Feedback[email]"]', form).val() || $('[name="Feedback[email]"]', form).val()) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res) {
                        $('.b-popup-error').hide();
                        $('.b-popup__content').hide();
                        $('.b-popup__thank-content').addClass('show');
                    }
                });
                
            } else {
                $('.b-popup-error').fadeIn(300);
                $('[name="Feedback[email]"]', form).trigger('focus');
            }

            return false;
        });

    }
/*
    $('.subscript__form').on('submit', function() {
        var form = $(this);
        var formData = new FormData(form[0]);

        if ($('[name="Feedback[email]"]', form).val()) {
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
            $('.subscript__email').removeClass('error');
            $('.subscript__error').hide();
            showAndHidePopup(false, false);
        } else {
            $('.subscript__email').addClass('error');
            $('.subscript__error').show();
        }

        return false;
    });*/

    // $('.subscript__form').on('submit', function () {
    //     if ($('[name="subscript"]').val()) {
    //         $('.subscript__email').removeClass('error');
    //         $('.subscript__error').hide();
    //         showAndHidePopup(false, false);
    //     } else {
    //         $('.subscript__email').addClass('error');
    //         $('.subscript__error').show();
    //     }
    //     return false;
    // });

    function showAndHidePopup(close, request) {
        
        var $popupWindow = $('.b-popup__content');
        $('.b-popup__wrp').toggleClass('show');
        $('body').css({overflowY: 'hidden'});
        

        if (close) {
            $('[name="phone"]').val('');
            $('[name="mail"]').val('');
            $('.b-popup-error').hide();
            $popupWindow.hide();
            $('.b-popup__wrp').removeClass('show');
            $('.b-popup__subscr-content').hide();
            $('.b-popup__thank-content').removeClass('show');
            $('body').css({overflowY: 'auto'});
            return true;
        }

        if (request) {
            $popupWindow.show();
        } else {
            $('.b-popup__subscr-content').show();
        }
    }
});