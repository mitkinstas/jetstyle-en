$(function() {
    // событие после успешной валидации
    $('.js-ajax_form').on('beforeSubmit', function() {
        var form = $(this);
        var formData = new FormData(form[0]);

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    form.find('progress').attr({ value: 0, max: 1 }).show();
                    myXhr.upload.addEventListener('progress', function(e) {
                        if (e.lengthComputable) {
                            form.find('progress').attr({ value: e.loaded, max: e.total });
                        }
                    }, false);
                }
                return myXhr;
            },
            success: function(res) {
                $(form.data('popup')).hide();
                $(form.data(res.success ? 'thank' : 'error')).addClass('show');

                form.trigger('reset');
                form.find('progress').hide();

//                ga('send', 'event', 'lead', 'sent', 'promo');
//                yaCounter4480927.reachGoal('LEAD_SENT_PROMO', yaGoalParams); 
            }
        });

        return false;
    });
});